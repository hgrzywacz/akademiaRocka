#(use-modules (guile-user))

\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a,, {
  \key c \major
  \time 4/4

  d2-0\2_"D" e-1\2_"E"
  f-2\2_"F" g-1\2_"G"
  a-4\2_"A" b-1\2_\bnote
  c-2\2_"C" d-4\2_"D"

  c-2\2_"C" b-1\2_\bnote
  a-4\2_"A" g-1\2_"G"
  f-2\2_"F" e-1\2_"E"
}

scoreElectricBassI = \new Staff \with {
  % midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseI

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>
  \layout {
    \context { \Voice
    \remove New_fingering_engraver
  }
}
\header {
}
}
