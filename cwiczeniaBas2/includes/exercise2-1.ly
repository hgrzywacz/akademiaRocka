#(use-modules (guile-user))

\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a, {
  \key c \major
  \time 4/4

  g2-0\1_"G"
  a-1\1_"A" b-4\1_\bnote
  c-1\1_"C" d-4\1_"D"
  e-1\1_"E" f-2\1_"F"

  g-4\1_"G"
  f-2\1_"F" e-1\1_"E"
  d-4\1_"D" c-1\1_"C"
  b-4\1_\bnote a-1\1_"A"
}

scoreElectricBassI = \new Staff \with {
  % midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseI

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
