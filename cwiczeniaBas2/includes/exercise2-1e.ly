#(use-modules (guile-user))

\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a,, {
  \key c \major
  \time 4/4

  e2-0\4_"E"

  f-1\4_"F"
  g-1\4_"G" a-4\4_"A"
  b-1\4_\bnote c-2\4_"C"
  d-1\4_"D"

  e-4\4_"E"

  d-1\4_"D"
  c-2\4_"C" b-1\4_\bnote
  a-4\4_"A" g-1\4_"G"
  f-1\4_"F"
}

scoreElectricBassI = \new Staff \with {
  % midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseI

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>
  \layout {
    \context { \Voice
    \remove New_fingering_engraver
  }
}
\header {
}
}
