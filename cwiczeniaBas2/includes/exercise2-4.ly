\version "2.18.2"
\include "english.ly"

exerciseIV = \relative a {
  \key c \major
  \time 4/2

  b,2-1\2 fsharp'-3\1
  c-2\2 g'-4\1

  csharp,-3\2 e-1\1
  d-4\2 f-2\1

}

scoreElectricBassIV = \new Staff \with {
  % midiIInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseIV
}

tabElectricBassIV = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseIV

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassIV
    \tabElectricBassIV
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
