\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseII = \relative a, {
  \key c \major
  \time 3/2

  g2-0\1
  a-1\1_"A" b-4\1_"B"

  g-0\1
  b-4\1_"B" c-1\1_"C"

  g-0\1
  c-1\1_"C" d-4\1_"D"

  g,-0\1
  d'-4\1_"D" e-1\1_"E"

  g,-0\1
  e'-1\1_"E" f-2\1_"F"

  g,-0\1
  f'-2\1_"F" g-4\1_"G"

  \bar "|" \break

  g,-0\1
  f'-2\1_"F" g-4\1_"G"

  g,-0\1
  e'-1\1_"E" f-2\1_"F"

  g,-0\1
  d'-4\1_"D" e-1\1_"E"

  g,-0\1
  c-1\1_"C" d-4\1_"D"

  g,-0\1
  b-4\1_"B" c-1\1_"C"

  g-0\1
  a-1\1_"A" b-4\1_"B"
}

scoreElectricBassII = \new Staff \with {
  % midiIInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseII
}

tabElectricBassII = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseII

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassII
    \tabElectricBassII
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
