\version "2.18.2"
\include "english.ly"

exerciseIII = \relative a, {
  \key c \major
  \time 3/2

  g2-0\1
  b-4\1_"B" a-1\1_"A"

  g-0\1
  c-1\1_"C" b-4\1_"B"

  g-0\1
  d'-4\1_"D" c-1\1_"C"

  g-0\1
  e'-1\1_"E" d-4\1_"D"

  g,-0\1
  f'-2\1_"F" e-1\1_"E"

  g,-0\1
  g'-4\1_"G" f-2\1_"F"

  \bar "|" \break

  g,-0\1
  g'-4\1_"G" f-2\1_"F"

  g,-0\1
  f'-2\1_"F" e-1\1_"E"

  g,-0\1
  e'-1\1_"E" d-4\1_"D"

  g,-0\1
  d'-4\1_"D" c-1\1_"C"

  g-0\1
  c-1\1_"C" b-4\1_"B"

  g-0\1
  b-4\1_"B" a-1\1_"A"
}


scoreElectricBassIII = \new Staff \with {
  % midiIInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseIII
}

tabElectricBassIII = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseIII

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassIII
    \tabElectricBassIII
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
