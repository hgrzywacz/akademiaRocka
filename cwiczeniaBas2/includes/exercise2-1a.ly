#(use-modules (guile-user))

\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a,, {
  \key c \major
  \time 4/4

  a2-0\3_"A" b-1\3_\bnote c-2\3_"C"
  d-1\3_"D"
  e-1\3_"E" f-2\3_"F"
  g-1\3_"G" a-4\3_"A"

  g-1\3_"G"
  f-2\3_"F" e-1\3_"E"
  d-1\3_"D"
  c-2\3_"C" b-1\3_\bnote
}

scoreElectricBassI = \new Staff \with {
  % midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseI

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>
  \layout {
    \context { \Voice
    \remove New_fingering_engraver
  }
}
\header {
}
}
