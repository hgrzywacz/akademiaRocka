#(use-modules (guile-user))

\version "2.18.2"
\include "english.ly"

exerciseIV = \relative a,, {
  \key c \major
  \time 4/4
  c2-2\3_"C" d-4\3_"D"
  e-1\2_"E" f-2\2_"F" g-4\2_"G"
  a-1\1_"A" b-3\1_\bnote c-4\1_"C"
  b-3\1_\bnote a-1\1_"A"
  g-4\2_"G" f-2\2_"F" e-1\2_"E"
  d-4\3_"D"
}

scoreElectricBassIV = \new Staff \with {
  % midiIInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseIV
}

tabElectricBassIV = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseIV

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassIV
    \tabElectricBassIV
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
