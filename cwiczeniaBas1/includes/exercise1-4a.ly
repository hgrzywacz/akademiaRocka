#(use-modules (guile-user))

\version "2.18.2"
\include "english.ly"

exerciseIV = \relative a,, {
  \key c \major
  \time 4/4

  c2-2\4_"C" d-4\4_"D"
  e-1\3_"E" f-2\3_"F" g-4\3_"G"
  a-1\2_"A" b-3\2_\bnote c-4\2_"C"
  b-3\2_\bnote a-1\2_"A"
  g-4\3_"G" f-2\3_"F" e-1\3_"E"
  d-4\4_"D"
}

scoreElectricBassIV = \new Staff \with {
  % midiIInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseIV
}

tabElectricBassIV = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseIV


\score {
  <<
    \scoreElectricBassIV
    \tabElectricBassIV
  >>
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
