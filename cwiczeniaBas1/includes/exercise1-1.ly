\version "2.18.2"
\include "english.ly"

exerciseI = \relative a, {
  \key c \major
  \time 6/4
  \repeat volta 2 {g2-0\1 asharp-1\1 c-4\1}
  \repeat volta 2 {d,-0\2 f-1\2 g-4\2}
  \repeat volta 2 {a,-0\3 c-1\3 d-4\3}
  \repeat volta 2 {e,-0\4 g-1\4 a-4\4}
}

scoreElectricBassI = \new Staff \with {
  % midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseI

% jeżeli string piece jest zdefiniowany to go używa, jeżeli nie to zwraca pusty string
#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context {\Voice
      \remove New_fingering_engraver
    }
  }
}
