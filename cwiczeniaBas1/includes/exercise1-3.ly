\version "2.18.2"
\include "english.ly"

exerciseIII = \relative a, {
  \key c \major
  \time 6/4

  c2-4\1 b-2\1 asharp-1\1 b-2\1 c2-4\1 b-2\1
  g-4\2 fsharp-2\2 f-1\2 fsharp-2\2 g-4\2 fsharp-2\2
  d-4\3 csharp-2\3 c-1\3 csharp-2\3 d-4\3 csharp-2\3
  a-4\4 gsharp-2\4 g-1\4 gsharp\4 a-4\4 gsharp-2\4
  d'-4\3 csharp-2\3 c-1\3 csharp-2\3 d-4\3 csharp-2\3
  g'-4\2 fsharp-2\2 f-1\2 fsharp-2\2 g-4\2 fsharp-2\2
  c'-4\1 b-2\1 asharp-1\1 b-2\1 c2-4\1 b-2\1
}

scoreElectricBassIII = \new Staff \with {
  % midiIInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseIII
}

tabElectricBassIII = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseIII

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassIII
    \tabElectricBassIII
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
