\version "2.18.2"
\include "english.ly"

exerciseII = \relative a, {
  \key c \major
  \time 6/4
  \repeat volta 2 {g2-0\1 c-4\1 asharp-1\1}
  \repeat volta 2 {d,-0\2 g-4\2 f-1\2}
  \repeat volta 2 {a,-0\3 d-4\3 c-1\3}
  \repeat volta 2 {e,-0\4 a-4\4 g-1\4}
}

scoreElectricBassII = \new Staff \with {
  % midiIInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseII
}

tabElectricBassII = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseII

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassII
    \tabElectricBassII
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
