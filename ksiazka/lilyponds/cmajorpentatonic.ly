\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a,, {
  \key c \major
  \time 6/2
  c2 d e g a c
}

scoreElectricBassI = \new Staff \with {
  % midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

\score {
  <<
    \scoreElectricBassI
  >>
}
