#! /usr/bin/env bash
lilypond-book -f latex --output=build/ \
  --process "lilypond -e '(define bnote \"B\")'" \
  --pdf ksiazka.lytex

cd build/
pdflatex -shell-escape --halt-on-error ksiazka.tex
mv ksiazka.pdf ../
cd ..
