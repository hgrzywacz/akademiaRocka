Bas od podstaw
======================

### Instalacja zależności ###

    $ sudo apt install texlive ghc lilypond inotify-tools

### Builder ###

    $ ./watch.hs

Skrypt który obserwuje pliki `.ly` i `.lytex` i buduje cały projekt do katalogu `build/`.

### Struktura ###

W katalogach `cwiczeniaBasN` są arkusze w lilypondzie które zbierają ćwiczenia z
katalogów include dla kolejnych lekcji. W katalogu `ksiazka` jest plik latex z zebranymi ćwiczeniami, niektóre rozdziały są w `ksiazka/includes`. Wszystkie N+1 pdf'ów buduje się do katalogu build.

      .
      ├── cwiczeniaBas1
      │   ├── cwiczeniaBas1.ly <-- arkusze z kolejnych lekcji
      │   └── includes
      ├── cwiczeniaBas2
      │   ├── (...)
      ├── ksiazka
      │   ├── includes
      │   ├── ksiazka.lytex <-- książka
      └── build
          ├── cwiczeniaBas1.pdf
          ├── (...)
          └── ksiazka.pdf
