\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a, {
  \key d \major
  \time 4/4

  \repeat volta 2 {
    d,4\3 fsharp a\2 b
    d,4\3 fsharp a\2 b

    g\3 b\2 d\2 e
    c\2 c\2 b\2 b\2

    d,4\3 fsharp a\2 b
    d,4\3 fsharp a\2 b

    g\3 b\2 d\2 e
    c\2 c\2 b\2 b\2

    d,4\3 fsharp a\2 b
    asharp,\3 asharp c\3 c\3
    d4\3 fsharp a\2 b
    d,1\3
  }

  g,4\4 e'\2 d\2 e\2
  g,4\4 e'\2 d\2 e\2
  c\3 c\3 c\3 c\3

  g\4 e'-1\2 d\2 e-2\2

  asharp\1 a\1 g\1 f\2
  dsharp\2 d\2 c\3 asharp\3

  e'\2 e\2 a\3 a\3
  e,1\4

}

scoreElectricBassI = \new Staff \with {
  midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseI

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}

\score {
  <<
    \scoreElectricBassI
  >>
  \midi {
    \tempo 4=154
  }
}
