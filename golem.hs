#! /usr/bin/env runhaskell

{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module Golem where

import Control.Monad
import Control.Exception

import Control.Concurrent.Async

import Control.Monad.Trans.Except
import Control.Monad.Trans.Class

import System.Directory
import System.Process
import System.Exit
import System.FilePath.Posix
import System.Console.ANSI
import System.Environment (getArgs)

import Data.List
import Data.ByteString.Lazy (ByteString, pack)
import Data.Either (rights)

-- flip fmap
(&) = flip ($)
($>) = flip ($)

v >$> f = fmap f v
infixl 1 >$>

type StdOut = String
type StdErr = String

class Output a where
  exitCode :: a -> ExitCode
  intExitCode :: a -> Maybe Int
  stdout :: a -> StdOut
  stderr :: a -> StdErr
  isZeroCode :: a -> Bool
  isZeroCode out = (intExitCode out) == Just 0

-- OutputWithExitCode - (exit code, stdout, strerr)
type OutputWithExitCode = (ExitCode, StdOut, StdErr)

-- SuccessOutput - (stdout, stderr)
newtype SuccessOutput = SuccessOutput (StdOut, StdErr) deriving Show

instance Output OutputWithExitCode where
  exitCode (ec, _, _) = ec
  intExitCode (ExitSuccess, _, _) = Nothing
  intExitCode ((ExitFailure n), _, _) = Just n
  stdout (_, so, _) = so
  stderr (_, _, se) = se

instance Output SuccessOutput where
  exitCode = const ExitSuccess
  intExitCode = const Nothing
  isZeroCode = const True
  stdout (SuccessOutput (so, _)) = so
  stderr (SuccessOutput (_, se)) = se

type ExceptIO = ExceptT OutputWithExitCode IO

mapConcurrentlyEI :: (a -> ExceptIO b) -> [a] -> ExceptIO [b]
mapConcurrentlyEI f xs = lift $ do
  as <- mapConcurrently (runExceptT . f) xs
  return $ rights as

-- rozszerzenia które nas interesują
extensions :: [String]
extensions = [".ly", ".lytex"]

bookDirectory :: String
bookDirectory = "ksiazka"

-- katalog do którego mają się przenieść pliki
buildDirectoryName :: String
buildDirectoryName = "build"

-- argumenty do inotifywait
inotifyOptions :: [String]
inotifyOptions =
  ["-q",                              -- quiet
   "-e", "create,modify,close_write", -- eventy do obserwowania
   "--fromfile", "-",                 -- pliki do obserwowania ze standardowego wejścia `-'
   "--format", "'%w%f'"]              -- format w którym ma być przekazywana dalej ścieżka

getBuildDirectory :: IO FilePath
getBuildDirectory = do
  cwd <- getCurrentDirectory
  let path = combine cwd buildDirectoryName
  createDirectoryIfMissing False path
  return path

hasLyExtension :: FilePath -> Bool
hasLyExtension path = any (hasSuffix path) extensions
  where
    hasSuffix = flip isSuffixOf

getFromDirectory :: FilePath -> IO [FilePath]
getFromDirectory directory = withCurrentDirectory directory action
  where
    action = do
      if directory == "build"
        then return []
        else do
          entries <- listDirectory "."
          directories <- filterM doesDirectoryExist entries

          subDirectories <- mapM getFromDirectory directories
          let lyFiles = filter hasLyExtension entries
          canonFiles <- mapM canonicalizePath lyFiles
          let subLyFiles = concat subDirectories

          return (canonFiles ++ subLyFiles)

getFromDirectoryStdout :: FilePath -> IO String
getFromDirectoryStdout directory = getFromDirectory directory >$> unlines

getFromCurrentDirectory :: IO String
getFromCurrentDirectory = getCurrentDirectory >>= getFromDirectoryStdout

inotify :: [String] -> ExceptIO String
inotify options = ExceptT $ do
  filesToWatch <- getFromCurrentDirectory
  output <- readProcessWithExitCode "inotifywait" options filesToWatch

  case exitCode output of
    ExitSuccess -> return $ Right $ stderr output
    ExitFailure 1 -> return $ Right $ stderr output
    ExitFailure n -> return $ Left $ output

-- ścieżka do pliku który się zmienił
fileChange :: ExceptIO FilePath
fileChange =  (inotify inotifyOptions) >$> cleanPath
  where
    cleanPath = filter ('\'' /=) . filter ('\n' /=)

wrapInEither :: OutputWithExitCode -> Either OutputWithExitCode SuccessOutput
wrapInEither (ExitSuccess, so, se) = Right $ SuccessOutput (so, se)
wrapInEither output = Left output

returnOutput :: OutputWithExitCode -> IO (Either OutputWithExitCode SuccessOutput)
returnOutput = return . wrapInEither

movePdf :: String -> FilePath  -> ExceptIO SuccessOutput
movePdf buildDir directory = ExceptT $ do

  let pdfFilepath = addExtension directory "pdf"
  putStrLn $ "\nPrzenoszenie " ++ pdfFilepath ++ " do ../build.\n"

  output <- readProcessWithExitCode "mv" [pdfFilepath , buildDir] ""

  returnOutput output

bNote :: Naming -> String
bNote English = "B"
bNote German = "H"

lilypondOptions :: (Handness, Naming) -> FilePath -> [String]
lilypondOptions (handness, naming) fp = ["-e",
                                         "(define bnote \"" ++ (bNote naming) ++ "\")",
                                         fp]

lilypondCompile :: (Handness, Naming) -> FilePath -> ExceptIO SuccessOutput
lilypondCompile parameters directory = ExceptT $ do

  let filepath = combine directory $ addExtension directory ".ly"
  output <-  readProcessWithExitCode "lilypond" (lilypondOptions parameters filepath) ""

  putStrLn $ (stderr output) ++ "\n"

  returnOutput output

lilypondAction :: (Handness, Naming) -> String -> FilePath -> ExceptIO SuccessOutput
lilypondAction parameters buildDirectory directory = do
  lilypondCompile parameters directory
  movePdf buildDirectory directory

cowsay :: String -> IO (ExitCode, String, String) -- (Either OutputWithExitCode SuccessOutput)
cowsay message = readProcessWithExitCode "cowsay" [message] ""

moveBookPdf :: String -> FilePath -> ExceptIO SuccessOutput
moveBookPdf buildDir directory  = ExceptT $ withCurrentDirectory (combine directory "build") $ do
  let pdfFile = addExtension directory ".pdf"

  cowsayOutput <- cowsay $ "\n" ++ pdfFile ++ " przeniesiona do ../build!"

  putStrLn $ (stdout cowsayOutput) ++ "\n"

  output <- readProcessWithExitCode "mv" [pdfFile, buildDir] ""

  returnOutput output

buildPdf :: String -> ExceptIO SuccessOutput
buildPdf directory = ExceptT $ withCurrentDirectory (combine directory "build") $ do
  let texFile = addExtension directory ".tex"
  output <- readProcessWithExitCode "pdflatex" ["--halt-on-error", "-shell-escape", texFile] ""
  putStrLn $ (stderr output)

  returnOutput output

lilypondBookOptions :: (Handness, Naming) -> String -> [String]
lilypondBookOptions (handness, naming) cwd =
  ["-f", "latex",
   "--output=build/", "--pdf",
   "--process", "lilypond -e '(define bnote \"" ++ (bNote naming) ++ "\")'",
   cwd ++ ".lytex"]

buildTex :: (Handness, Naming) -> FilePath -> ExceptIO SuccessOutput
buildTex parameters directory = ExceptT $ withCurrentDirectory directory $ do
  output <- readProcessWithExitCode "lilypond-book" (lilypondBookOptions parameters directory) ""
  putStrLn $ (stderr output)

  returnOutput output

bookAction :: (Handness, Naming) -> String -> FilePath -> ExceptIO SuccessOutput
bookAction parameters buildDir directory = do
  buildTex parameters directory
  buildPdf directory
  moveBookPdf buildDir directory

coerceExceptions :: SomeException -> IO (Either OutputWithExitCode b)
coerceExceptions e = return $ Left (ExitFailure 10, "", "Wystąpił wyjątek: " ++ (show e))

listDirectories :: FilePath -> ExceptT OutputWithExitCode IO [FilePath]
listDirectories path = ExceptT $ catch (action >$> Right) coerceExceptions
  where
    action = withCurrentDirectory path (listDirectory path >>= (filterM doesDirectoryExist))

doListDirectories :: ExceptT OutputWithExitCode IO [FilePath]
doListDirectories = (lift getCurrentDirectory) >>= listDirectories

-- czy katalog kończy się cyfrą
isLilypondDirectory :: FilePath -> Bool
isLilypondDirectory filePath = elem (last filePath) ['0'..'9']

-- czy katalog jest z książką
isBookDirectory :: FilePath -> Bool
isBookDirectory filePath = filePath == bookDirectory

mapDirectoriesToActions :: (Handness, Naming) -> String -> [FilePath] -> ExceptIO SuccessOutput
mapDirectoriesToActions parameters buildDirectory filePaths = do

  let lyPaths = filter isLilypondDirectory filePaths $> sort

  let bookPath = filter isBookDirectory filePaths $> head

  let lyActions = mapConcurrentlyEI (lilypondAction parameters buildDirectory) lyPaths

  results <- lyActions

  bookAction parameters buildDirectory bookPath

  -- mapConcurrentlyEI (\path ->
  --   if (isLilypondDirectory path) then
  --     lilypondAction parameters buildDirectory path
  --   else
  --     bookAction parameters buildDirectory path)

doMapDirectoriesToAction :: (Handness, Naming) -> ExceptIO SuccessOutput
doMapDirectoriesToAction parameters = do
  buildDirectory <- lift $ getBuildDirectory
  directories <- doListDirectories
  mapDirectoriesToActions parameters buildDirectory directories

compile :: (Handness, Naming) -> ExceptIO SuccessOutput
compile parameters = doMapDirectoriesToAction parameters

resetColor :: IO ()
resetColor = setSGR [Reset]

setColor :: Color -> IO ()
setColor color = do
    setSGR [SetColor Foreground Dull color]

logSuccess :: IO ()
logSuccess = do
  setSGR [SetColor Foreground Vivid Green]
  putStrLn "Zrobione!"
  setSGR [Reset]

logError :: OutputWithExitCode -> IO ()
logError (ExitSuccess, _, _) = logSuccess
logError ((ExitFailure n), _, se) = do
  setSGR [SetColor Foreground Vivid Red]
  putStrLn $ "\nBłąd! (" ++ show n ++ "):\n"
  setSGR [Reset]

logErrors :: [OutputWithExitCode] -> IO ()
logErrors outputs = sequence_ $ map logError outputs

watchAndCompile :: (Handness, Naming) -> ExceptIO SuccessOutput
watchAndCompile parameters = do
  path <- fileChange

  let extension = takeExtension path
  let splitPath = splitFileName path

  buildDirectory <- lift getBuildDirectory

  lift $ putStrLn path

  compile parameters

compileReport :: (Handness, Naming) -> IO ()
compileReport parameters = do
  output <- runExceptT $ compile parameters
  case output of
    Left output -> logError output
    Right _ -> logSuccess

watchCompileReport :: (Handness, Naming) -> IO ()
watchCompileReport parameters = do
  output <- runExceptT $ watchAndCompile parameters

  case output of
    Left output -> logError output
    Right _ -> logSuccess

data Handness = RightHand | LeftHand deriving Show
data Naming = English | German deriving Show

data Action = Compile | Watch

parseHandness :: [String] -> Handness
parseHandness args | elem "left" args = LeftHand
                   | otherwise = RightHand

parseNaming :: [String] -> Naming
parseNaming args | elem "en" args = English
                 | otherwise = German

parseAction :: [String] -> Action
parseAction args | elem "watch" args = Watch
                 | otherwise = Compile

parseParameters :: [String] -> (Handness, Naming)
parseParameters args = ((parseHandness args), (parseNaming args))

main :: IO ()
main = do
  args <- getArgs
  let action = parseAction args
  let parameters = parseParameters args

  case action of
      Compile -> compileReport parameters
      Watch   -> compileReport parameters >> forever (watchCompileReport parameters)
