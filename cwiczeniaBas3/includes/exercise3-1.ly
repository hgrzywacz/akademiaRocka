\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a, {
  \key c \major
  \time 4/4

  a1
  a2 a
  a4 a a a
  a8 a a a a a a a
  a16 a a a a a a a a a a a a a a a
  a8 a a a a a a a
  a4 a a a
  a2 a
  a1

}

scoreElectricBassI = \new Staff \with {
  % midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassI
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
