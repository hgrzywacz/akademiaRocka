\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

chordsline = \chordmode {
  \set chordNameExceptions = #ignatzekExceptions

  \key c \major
  \time 4/4

  c1:maj7
  d:m7
  e:m7
  f:maj7
  g:7
  a:m7
  b:m7.5-
  c':maj7
}

chordsNames = \new ChordNames {
  \chordsline
}

chordsFretboards = \new FretBoards \with { % \set Staff.stringTunings = #ukulele-tuning
  stringTunings = #guitar-tuning
} {
  \chordsline
}

scoreElectricGuitar = \new Staff {
  \clef "violin" \chordsline
}

\score {
  <<
    \chordsNames
    \chordsFretboards
    \scoreElectricGuitar
  >>
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
