\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

chordsline = \chordmode {
  \set chordNameExceptions = #ignatzekExceptions

  \key c \major
  \time 4/4

  c,,:maj7
  d,,:m7
  e,,:m7
  f,,:maj7
  g,,:7
  a,,:m7
  b,,:m7.5-
  c,:maj7
}

chordsNames = \new ChordNames {
  \chordsline
}

scoreChordNotes = \new Staff {
  \clef "bass_8" \chordsline
}

bassline = \relative a,, {
  \key c \major
  \time 4/4

  % c d e f g a b | c d e f g a b

  <c\3 e\2 b'\1> % Cmaj7
  <d\3 f\2 c'\1> % Dmin7
  <e\3 g\2 d'\1> % Emin7
  <f\3 a\2 e'\1> % Fmaj7
  <g\3 b\2 f'\1> % G7
  <a\3 c\2 g'\1> % Amin7
  <b\3 d\2 a'\1> % Bm7b5
  <c\3 e\2 b'\1> % Cmaj7

}

chordsFretboards = \new FretBoards \with { % \set Staff.stringTunings = #ukulele-tuning
  stringTunings = #bass-tuning
} {
  \bassline
}

scoreElectricBass = \new Staff {
  \clef "bass_8" \bassline
}

tabElectricBass = \new TabStaff \with {
  stringTunings = #bass-tuning
} \bassline

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \chordsNames
    \chordsFretboards
    \scoreChordNotes
    \scoreElectricBass
    \tabElectricBass
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
