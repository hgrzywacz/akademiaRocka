\version "2.18.2"
\include "english.ly"
\include "predefined-guitar-fretboards.ly"

\header {
  title = "Bas - ćwiczenia V"
  composer = "by Marcin Jadach @ akademiarocka.pl"
  arranger = "spisał i złożył: Hubert Grzywacz (hgrzywacz@gmail.com)"
  tagline = "Złożone przy pomocy LilyPond 2.18.2 (www.lilypond.org)"
}

global = {
  \key c \major
  \time 6/4
}

\layout {
  indent = #0
  ragged-last = ##f
}

chExceptionMusic = {
  <c e g b d'>1-\markup { \super "maj9" }
  % F#m7b5 - F sharp minor seventh flat fifth
  <c eflat gflat bflat >1-\markup { \super {"m7" \flat "5"}}
  <c eflat gflat bflat aflat'>1-\markup { \super {"alt7"}}
}

% Convert music to list and prepend to existing exceptions.
chExceptions = #( append
  ( sequential-music-to-chord-exceptions chExceptionMusic #t))

piece = "Skala c major tercjami"
\include "includes/exercise5-1.ly"

piece = "Skala c major kwintami"
\include "includes/exercise5-2.ly"
