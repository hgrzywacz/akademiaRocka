\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseII = \relative a,, {
  \key c \major
  \time 2/4

  c4_"C" f_"F"\2
  d_"D" g_"G"
  e_"E" a_"A"
  f_"F" b_\bnote
  g_"G" c_"C"
  a_"A" d_"D"
  b_\bnote e_"E"

  \break

  d_"D" a_"A"
  c_"C" g_"G"
  b_\bnote f_"F"
  a_"A" e_"E"
  g_"G" d_"D"
  f_"F" c_"C"

}

scoreElectricBassI = \new Staff \with {
  midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseII
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseII

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
