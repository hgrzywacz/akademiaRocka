#(use-modules (guile-user))

\version "2.18.2"
\include "english.ly"

\layout {
  indent = #0
  ragged-last = ##f
}

exerciseI = \relative a,, {
  \key c \major
  \time 2/4

  c4_"C" e_"E"
  d_"D"\3 f_"F"
  e_"E" g_"G"\2
  f_"F" a_"A"
  g_"G"\2 b_\bnote
  a_"A" c_"C"
  b_\bnote d_"D"\1

  \break

  c_"C" a_"A"
  b_\bnote g_"G"\2
  a_"A" f_"F"
  g_"G"\2 e_"E"
  f_"F" d_"e"\3
  e_"E" c_"C"
  d_"D"\3
  b_\bnote\3 c2_"C"

}

scoreElectricBassI = \new Staff \with {
  midiInstrument = "electric bass (finger)"
} {
  \clef "bass_8" \exerciseI
}

tabElectricBassI = \new TabStaff \with {
  stringTunings = #bass-tuning
  \consists "Instrument_name_engraver"
} \exerciseI

#(define piece (if (eqv? (ly:parser-lookup parser 'piece) '()) "" piece))

\score {
  <<
    \scoreElectricBassI
    \tabElectricBassI
  >>

  \header {
    piece = \piece
  }
  \layout {
    \context { \Voice
      \remove New_fingering_engraver
    }
  }
}
